<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Translation\Translator;

class ReaderType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $translator = new Translator('en');
        $builder->add('name', TextType::class, array(
            'label' => false,
            'attr' => array(
                'placeholder' => $translator->trans('Name'),
            ),
        ))
            ->add('address', TextType::class, array(
                'label' => false,
                'attr' => array(
                    'placeholder' => $translator->trans('Address'),
                ),
            ))
            ->add('passport', TextType::class, array(
                'label' => false,
                'attr' => array(
                    'placeholder' => $translator->trans('Passport'),
                ),
            ))
            ->add('save', SubmitType::class,
                array('label' => $translator->trans('Apply'),
                    'attr' => ["class" => "btn btn-success"]
                ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {

    }


    public function getBlockPrefix()
    {
        return 'app_bundle_reader_type';
    }
}
