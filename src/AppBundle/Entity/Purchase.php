<?php

namespace AppBundle\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * Order
 *
 * @ORM\Table(name="purchase")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PurchaseRepository")
 */
class Purchase
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="recive_date", type="datetime")
     */
    private $recive_date;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="expire_date", type="datetime")
     */
    private $expire_date;


    /**
     * @var Reader
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Reader", inversedBy="takeBook")
     * @ORM\JoinColumn(name="reader_id", referencedColumnName="id")
     */
    private $reader;

    /**
     * @var Book
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Book", inversedBy="usersTakes")
     * @ORM\JoinColumn(name="book_id", referencedColumnName="id")
     */
    private $book;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set reciveDate
     *
     * @param \DateTime $reciveDate
     *
     * @return Purchase
     */
    public function setReciveDate(\DateTime $reciveDate)
    {
        $this->recive_date = $reciveDate;

        return $this;
    }

    /**
     * Get reciveDate
     *
     * @return \DateTime
     */
    public function getReciveDate()
    {
        return $this->recive_date;
    }

    /**
     * Set expireDate
     *
     * @param \DateTime $expireDate
     *
     * @return Purchase
     */
    public function setExpireDate(\DateTime $expireDate)
    {
        $this->expire_date = $expireDate;

        return $this;
    }

    /**
     * Get expireDate
     *
     * @return \DateTime
     */
    public function getExpireDate()
    {
        return $this->expire_date;
    }

    /**
     * @param Reader $reader
     * @return Purchase
     */
    public function setReader($reader): Purchase
    {
        $this->reader = $reader;
        return $this;
    }

    /**
     * @return Reader
     */
    public function getReader()
    {
        return $this->reader;
    }

    /**
     * @param Book $book
     * @return Purchase
     */
    public function setBook(Book $book): Purchase
    {
        $this->book = $book;
        return $this;
    }

    /**
     * @return Book
     */
    public function getBook()
    {
        return $this->book;
    }
}

