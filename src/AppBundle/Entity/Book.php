<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\HttpFoundation\File\File;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Book
 *
 * @ORM\Table(name="book")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\BookRepository")
 * @Vich\Uploadable
 */
class Book
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string")
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="author", type="text")
     */
    private $author;

    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string")
     */
    private $image;

    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Category", inversedBy="books", cascade={"persist", "remove"})
     */
    private $categories;

    /**
     * @var mixed
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Purchase", mappedBy="book")
     */
    private $usersTakes;

    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     *
     * @Vich\UploadableField(mapping="book_image", fileNameProperty="image")
     *
     * @Assert\File(
     *     mimeTypes = { "image/gif", "image/jpeg", "image/png" },
     *     mimeTypesMessage = "Недопустимый тип данных ({{ type }}). Допустимы: {{ types }}.",
     *     maxSize = "2000k",
     *     maxSizeMessage = "Размер файла привышает 2 мегабайта"
     * )
     * @Assert\Image(
     *     minRatio = "0.55",
     *     maxRatio = "1.82",
     *     minHeight = "300",
     *     minWidth = "300"
     * )
     * @var File
     */
    private $imageFile;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="text")
     */
    private $status;


    public function __construct()
    {
        $this->categories = new ArrayCollection();
        $this->usersTakes = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Book
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set author
     *
     * @param string $author
     *
     * @return Book
     */
    public function setAuthor($author)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get author
     *
     * @return string
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * Set image
     *
     * @param string $image
     *
     * @return Book
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image
     *
     * @return Book
     */
    public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;
        return $this;
    }

    /**
     * @return File|null
     */
    public function getImageFile()
    {
        return $this->imageFile;
    }

    public function __toString()
    {
        return $this->title ?: '';
    }

    /**
     * @return mixed
     */
    public function getCategories()
    {
        return $this->categories;
    }


    public function addCategories($category)
    {
        $this->categories->add($category);
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUsersTakes()
    {
        return $this->usersTakes;
    }

    public function addReaders(Reader $reader)
    {
        $this->usersTakes->add($reader);
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param string $status
     * @return Book
     */
    public function setStatus(string $status): Book
    {
        $this->status = $status;
        return $this;
    }
}

