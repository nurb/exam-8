<?php
namespace AppBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="user")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    protected $newPass;

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param mixed $newPass
     * @return User
     */
    public function setNewPass($newPass)
    {
        $this->newPass = $newPass;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getNewPass()
    {
        return $this->newPass;
    }
}
