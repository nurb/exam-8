<?php

namespace AppBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Reader
 *
 * @ORM\Table(name="reader")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ReaderRepository")
 */
class Reader
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=128)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="text", length=1024)
     */
    private $address;

    /**
     * @var string
     *
     * @ORM\Column(name="passport", type="string", length=255, unique=true)
     */
    private $passport;

    /**
     * @var string
     *
     * @ORM\Column(name="library_card", type="string")
     */
    private $library_card;

    /**
     * @var mixed
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Purchase", mappedBy="reader")
     */
    private $takeBook;

    public function __construct()
    {
        $this->takeBook = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Reader
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set address
     *
     * @param string $address
     *
     * @return Reader
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set passport
     *
     * @param string $passport
     *
     * @return Reader
     */
    public function setPassport($passport)
    {
        $this->passport = $passport;

        return $this;
    }

    /**
     * Get passport
     *
     * @return string
     */
    public function getPassport()
    {
        return $this->passport;
    }

    /**
     * Set libraryCard
     *
     * @param string $libraryCard
     *
     * @return Reader
     */
    public function setLibraryCard($libraryCard)
    {
        $this->library_card = $libraryCard;

        return $this;
    }

    /**
     * Get libraryCard
     *
     * @return string
     */
    public function getLibraryCard()
    {
        return $this->library_card;
    }

    /**
     * @return mixed
     */
    public function getTakeBook()
    {
        return $this->takeBook;
    }

    public function addBooks(Book $book)
    {
        $this->takeBook->add($book);
    }

    public function __toString()
    {
        return $this->name ?: '';
    }

}