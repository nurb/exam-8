<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Reader;
use AppBundle\Form\ReaderType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

class ReaderController extends Controller
{
    /**
     * @Route("/new_reader", name="new_reader")
     * @Method({"GET","HEAD", "POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function newReaderAction(Request $request)
    {
        $newReader = new Reader();
        $form = $this->createForm(ReaderType::class, $newReader, array(
            'method' => 'POST'
        ));
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $check = $this
                ->getDoctrine()
                ->getRepository('AppBundle:Reader')
                ->checkDuplicatePassport($newReader->getPassport());
            dump($check);
            if ($check !== null) {
                return $this->render('@App/Reader/success.html.twig', array(
                    "status" => "error",
                    "library_card" => null
                ));
            }


            $newReader->setLibraryCard(md5($newReader->getPassport()));
            $em = $this->getDoctrine()->getManager();
            $em->persist($newReader);
            $em->flush();
            return $this->render('@App/Reader/success.html.twig', array(
                "status" => "Success",
                "library_card" => $newReader->getLibraryCard()
            ));
        }

        return $this->render('@App/Reader/new_reader.html.twig', array(
            'form' => $form->createView()
        ));
    }

}
