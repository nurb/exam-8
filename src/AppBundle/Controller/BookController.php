<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Purchase;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;


class BookController extends Controller
{
    /**
     * @Route("/book/{id}", requirements={"id": "\d+"}, name="get_book")
     * @param Request $request
     * @Method({"GET","HEAD", "POST"})
     * @param int $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getBookAction(Request $request, int $id)
    {
        $book = $this
            ->getDoctrine()
            ->getRepository('AppBundle:Book')
            ->find($id);

        $form = $this->createFormBuilder();
        $form->add('library_card', TextType::class, ['label' => $this->get('translator')->trans('Library Card')]);
        $form->add('book_id', HiddenType::class, ['label' => false, 'data' => $book->getId()]);
        $form->add('author', TextType::class, ['label' => $this->get('translator')->trans('Author'), 'required' => false,
            'attr' => ['placeholder' => $book->getAuthor(), 'readonly' => true]]);
        $form->add('title', TextType::class, ['label' => $this->get('translator')->trans('Title'), 'required' => false,
            'attr' => ['placeholder' => $book->getTitle(), 'readonly' => true,]]);
        $form->add('date', DateType::class, array(
                'widget' => 'choice',
                'years' => range(date('Y'), date('Y')),
                'months' => range(date('m'), 12),
                'days' => range(date('d'), 31),
            )
            , ['label' => $this->get('translator')->trans('Date')]);
        $form->add('save', SubmitType::class, ['label' => $this->get('translator')->trans('Apply'), 'attr' => array(
            'class' => 'btn btn-success',
        )]);

        $form = $form->getForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();

            $readers = $this
                ->getDoctrine()
                ->getRepository('AppBundle:Reader')
                ->geReaderByLibraryCard($data['library_card']);

            if($readers === null){
                return $this->render('@App/Reader/success.html.twig', array(
                    "status" => "error2",
                    "library_card" => null
                ));
            }

            $reader = $this
                ->getDoctrine()
                ->getRepository('AppBundle:Reader')
                ->find($readers['id']);

            $purchase = new Purchase();
            $purchase->setReader($reader);
            $purchase->setBook($book);
            $purchase->setReciveDate(new \DateTime(date("Y-m-d H:i:s", time())));
            $purchase->setExpireDate($data['date']);
            $em = $this
                ->getDoctrine()
                ->getManager();
            $book->setStatus('busy');

            $em->persist($purchase);
            $em->flush();

            return $this->redirectToRoute('homepage');
        }


        return $this->render('@App/Book/get_book.html.twig', array(
            'form' => $form->createView()
        ));
    }


    /**
     * @Route("/my_book", requirements={"id": "\d+"}, name="my_book")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function formMyBookAction(Request $request)
    {
        $form = $this->createFormBuilder();
        $form->add('library_card', TextType::class, ['label' => $this->get('translator')->trans('Library Card')]);
        $form->add('save', SubmitType::class, ['label' => $this->get('translator')->trans('Save'), 'attr' => array(
            'class' => 'btn btn-success',
        )]);

        $form = $form->getForm();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();

            $readers = $this
                ->getDoctrine()
                ->getRepository('AppBundle:Reader')
                ->geReaderByLibraryCard($data['library_card']);

            $reader = $this
                ->getDoctrine()
                ->getRepository('AppBundle:Reader')
                ->find($readers['id']);

            $allBooks = $this
                ->getDoctrine()
                ->getRepository('AppBundle:Book')
                ->getAllBooksByUser($reader);

            return $this->render('@App/Book/my_book.html.twig', array(
                'form' => $form->createView(),
                'books' => $allBooks
            ));
        }

        return $this->render('@App/Book/my_book.html.twig', array(
            'form' => $form->createView(),
            'books' => null
        ));


    }

}
