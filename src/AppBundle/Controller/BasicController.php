<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;


class BasicController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction()
    {
        $books = $this
            ->getDoctrine()
            ->getRepository('AppBundle:Book')
            ->findAll();
        $categories = $this
            ->getDoctrine()
            ->getRepository('AppBundle:Category')
            ->findAll();

        return $this->render('@App/Basic/index.html.twig', array(
            'books' => $books,
            'categories' => $categories
        ));
    }

    /**
     * @Route("language/{language}", name="setLanguage", requirements = {"_locale" : "en|ru"})
     * @param Request $request
     * @param $language
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function setLanguage(Request $request, $language)
    {
        $this->get('session')->set('_locale', $language);
        return $this->redirect($request->headers->get('referer'));
    }

    /**
     * @Route("/category/{id}", requirements={"id": "\d+"}, name="get_category")
     * @param int $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function categoryAction(Int $id)
    {
        $categories = $this
            ->getDoctrine()
            ->getRepository('AppBundle:Category')
            ->findAll();

        $category = $this
            ->getDoctrine()
            ->getRepository('AppBundle:Category')
            ->find($id);

        $books = $this
            ->getDoctrine()
            ->getRepository('AppBundle:Book')
            ->getBooksByCateogry($category);


        $c_books = [];
        foreach ($books as $c_book) {
            $c_books[] = $this
                ->getDoctrine()
                ->getRepository('AppBundle:Book')
                ->find($c_book['book']->getId());
        }

        return $this->render('@App/Basic/index.html.twig', array(
            'books' => $c_books,
            'categories' => $categories
        ));

    }

}
