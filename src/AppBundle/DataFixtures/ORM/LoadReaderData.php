<?php

namespace AppBundle\DataFixtures\ORM;


use AppBundle\Entity\Reader;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;


class LoadReaderData extends Fixture
{
    public const READER_ONE = 'Rick';
    public const READER_TWO = 'Morti';

    public function load(ObjectManager $manager)
    {
        $reader1 = new Reader();
        $reader1
            ->setName('Rick')
            ->setPassport('A2999')
            ->setAddress('Some Street 2')
            ->setLibraryCard('53e896121b4a94b9257e03fbc7663550');
        $manager->persist($reader1);

        $reader2 = new Reader();
        $reader2
            ->setName('Morty')
            ->setPassport('A5666')
            ->setAddress('Some Street 20')
            ->setLibraryCard('53e896121b4a94b8293e03fbc7663550');
        $manager->persist($reader2);

        $this->addReference(self::READER_ONE, $reader1);
        $this->addReference(self::READER_TWO, $reader2);

        $manager->flush();

    }

}
