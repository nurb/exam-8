<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Book;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class LoadBookData extends Fixture implements DependentFixtureInterface
{
    public const BOOK_ONE = 'Старик и море';
    public const BOOK_TWO = 'Великий Гэтсби';
    public const BOOK_THREE = 'Гарри Поттер';
    public const BOOK_FOUR = 'Над пропостью во Ржи';

    public function load(ObjectManager $manager)
    {
        $book1 = new Book();
        $book1
            ->setTitle('Старик и море')
            ->setAuthor('Эрнест Хемингуэй')
            ->setImage('sea.jpg')
            ->setStatus('free')
            ->addCategories($this->getReference(LoadCategoriesData::CAT_THREE));

        $manager->persist($book1);

        $book2 = new Book();
        $book2
            ->setTitle('Великий Гэтсби')
            ->setAuthor('Джон Фицджеральд')
            ->setImage('great.jpg')
            ->setStatus('busy')
            ->addCategories($this->getReference(LoadCategoriesData::CAT_TWO));


        $manager->persist($book2);

        $book3 = new Book();
        $book3
            ->setTitle('Гарри Поттер')
            ->setAuthor('Джона Роулинг')
            ->setImage('harrypotter.jpg')
            ->setStatus('busy')
            ->addCategories($this->getReference(LoadCategoriesData::CAT_ONE));

        $manager->persist($book3);

        $book4 = new Book();
        $book4
            ->setTitle('Над пропостью во Ржи')
            ->setAuthor('Джером Селинджер')
            ->setImage('catchintheray.jpg')
            ->setStatus('busy')
            ->addCategories($this->getReference(LoadCategoriesData::CAT_FOUR));

        $manager->persist($book4);

        $this->addReference(self::BOOK_ONE, $book1);
        $this->addReference(self::BOOK_TWO, $book2);
        $this->addReference(self::BOOK_THREE, $book3);
        $this->addReference(self::BOOK_FOUR, $book4);
        $manager->flush();

    }

    /**
     * This method must return an array of fixtures classes
     * on which the implementing class depends on
     *
     * @return array
     */
    function getDependencies()
    {
        return array(
            LoadCategoriesData::class
        );
    }

}
