<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Book;
use AppBundle\Entity\Purchase;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class LoadPurchaseData extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $purchase = new Purchase();
        $purchase->setReader($this->getReference(LoadReaderData::READER_ONE));
        $purchase->setBook($this->getReference(LoadBookData::BOOK_ONE));
        $purchase->setReciveDate(new \DateTime(date("Y-m-d H:i:s", time() - 1864000)));
        $purchase->setExpireDate(new \DateTime(date("Y-m-d H:i:s", time() - 864000)));

        $manager->persist($purchase);

        $purchase = new Purchase();
        $purchase->setReader($this->getReference(LoadReaderData::READER_ONE));
        $purchase->setBook($this->getReference(LoadBookData::BOOK_THREE));
        $purchase->setReciveDate(new \DateTime(date("Y-m-d H:i:s", time() - 1864000)));
        $purchase->setExpireDate(new \DateTime(date("Y-m-d H:i:s", time() - 864000)));

        $manager->persist($purchase);

        $purchase = new Purchase();
        $purchase->setReader($this->getReference(LoadReaderData::READER_ONE));
        $purchase->setBook($this->getReference(LoadBookData::BOOK_FOUR));
        $purchase->setReciveDate(new \DateTime(date("Y-m-d H:i:s", time() - 864000)));
        $purchase->setExpireDate(new \DateTime(date("Y-m-d H:i:s", time() + 9864000)));

        $manager->persist($purchase);

        $purchase = new Purchase();
        $purchase->setReader($this->getReference(LoadReaderData::READER_TWO));
        $purchase->setBook($this->getReference(LoadBookData::BOOK_ONE));
        $purchase->setReciveDate(new \DateTime(date("Y-m-d H:i:s", time() - 9864000)));
        $purchase->setExpireDate(new \DateTime(date("Y-m-d H:i:s", time() - 9664000)));

        $manager->persist($purchase);

        $purchase = new Purchase();
        $purchase->setReader($this->getReference(LoadReaderData::READER_TWO));
        $purchase->setBook($this->getReference(LoadBookData::BOOK_TWO));
        $purchase->setReciveDate(new \DateTime(date("Y-m-d H:i:s", time() - 1864000)));
        $purchase->setExpireDate(new \DateTime(date("Y-m-d H:i:s", time() - 664000)));

        $manager->persist($purchase);


        $manager->flush();

    }

    /**
     * This method must return an array of fixtures classes
     * on which the implementing class depends on
     *
     * @return array
     */
    function getDependencies()
    {
        return array(
            LoadBookData::class,
            LoadReaderData::class
        );
    }

}
