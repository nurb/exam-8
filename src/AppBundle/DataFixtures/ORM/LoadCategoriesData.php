<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Category;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;


class LoadCategoriesData extends Fixture
{
    public const CAT_ONE = 'Фантастика';
    public const CAT_TWO = 'Драма';
    public const CAT_THREE = 'Классика';
    public const CAT_FOUR = 'Современная литература';

    public function load(ObjectManager $manager)
    {
        $category1 = new Category();
        $category1
            ->setName('Фантастика');
        $manager->persist($category1);

        $category2 = new Category();
        $category2
            ->setName('Драма');
        $manager->persist($category2);

        $category3 = new Category();
        $category3
            ->setName('Классика');

        $manager->persist($category3);

        $category4 = new Category();
        $category4
            ->setName('Современная литература');

        $manager->persist($category4);


        $this->addReference(self::CAT_ONE, $category1);
        $this->addReference(self::CAT_TWO, $category2);
        $this->addReference(self::CAT_THREE, $category3);
        $this->addReference(self::CAT_FOUR, $category4);
        $manager->flush();

    }

}
