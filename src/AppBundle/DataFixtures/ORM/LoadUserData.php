<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\User;


use Doctrine\Bundle\FixturesBundle\ORMFixtureInterface;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadUserData implements FixtureInterface, ORMFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $user = new User();
        $user
            ->setUsername('admin')
            ->setUsernameCanonical('admin')
            ->setEmail('admin@gmail.com')
            ->setEmailCanonical('admin@gmail.com')
            ->setPlainPassword('qwerty')
            ->setEnabled(true)
            ->setRoles(array('ROLE_ADMIN'));

        $manager->persist($user);

        $user = new User();
        $user
            ->setUsername('rick')
            ->setUsernameCanonical('rick')
            ->setEmail('rick@gmail.com')
            ->setEmailCanonical('rick@gmail.com')
            ->setPlainPassword('qwerty')
            ->setEnabled(true)
            ->setRoles(array('ROLE_USER'));

        $manager->persist($user);

        $user = new User();
        $user
            ->setUsername('morty')
            ->setUsernameCanonical('morty')
            ->setEmail('morty@gmail.com')
            ->setEmailCanonical('morty@gmail.com')
            ->setPlainPassword('qwerty')
            ->setEnabled(true)
            ->setRoles(array('ROLE_USER'));

        $manager->persist($user);

        $manager->flush();

    }

}
